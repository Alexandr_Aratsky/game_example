#pragma once

#include "Interfaces.h"
#include "ConsoleIO.h"
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>

// ���cc �����
class Wall: public IDrowable, ITexture {
private:
	pointf _center;
	bool _hasHole;

	sf::ConvexShape _wall;
	sf::CircleShape _hole;

public:

	Wall(pointf center, bool isRight);
	~Wall() { }

	virtual void Draw(sf::RenderWindow& rw);
	virtual void LoadTexture(const sf::Texture& texture, ushort texType = ETex::Default);

	bool HasHole() const;
	void HasHole(bool val);
};

