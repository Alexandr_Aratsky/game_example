#include "Wall.h"
#include "Cell.h"

void Wall::Draw(sf::RenderWindow& rw)
{
	rw.draw(_wall);
	if (_hasHole) rw.draw(_hole);
}

void Wall::LoadTexture(const sf::Texture& texture, ushort texType)
{
	if (texType & (ETex::WallLeft | ETex::WallRight)) _wall.setTexture(&texture);
}

Wall::Wall(pointf center, bool isRight)
{
	_center = center;
	int coef;
	if (isRight) coef = -1;
	else coef = 1;
	_wall.setPointCount(4);
	_wall.setPoint(0, pointf(_center.x, _center.y - Cell::Height()/2));
	_wall.setPoint(1, pointf(_center.x - coef * Cell::Width()/2, _center.y));
	_wall.setPoint(2, pointf(_center.x - coef * Cell::Width()/2, _center.y - Cell::Height()));
	_wall.setPoint(3, pointf(_center.x, _center.y - 3 * Cell::Height()/2));
	_hasHole = false;
	_hole.setRadius(Cell::Width()/6);
	if (!isRight) _hole.setPosition(_center.x - Cell::Width()/2, _center.y - Cell::Height());
	else _hole.setPosition(_center.x, _center.y - Cell::Height());
	_hole.setFillColor(sf::Color::Black);
	_hole.setOutlineThickness(2);
	_hole.setOutlineColor(sf::Color(128,128,128));
}

bool Wall::HasHole() const
{
	return _hasHole;
}

void Wall::HasHole(bool val)
{
	_hasHole = val;
}

