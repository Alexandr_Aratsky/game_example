#pragma once

#include "Interfaces.h"
#include "CharatcerSprite.h"
#include "ConsoleIO.h"
#include "resource.h"
#include <SFML/Graphics/Texture.hpp>

// ����������� ����� ��������� ������ ����� ��� ����
class ACharacter : public IDrowable {
private:

	const unsigned _tickMax;
	unsigned _tickCount;

protected:

	static point v_sign(point p);

	bool _isMoving;

	point _current_cell;
	point _next_cell;
	point _target_cell;

	virtual void end_moving_handler() = 0;
	virtual void next_step() = 0;

	static IBoardInterface* _b;

public:

	ACharacter(unsigned speedTick);
	virtual ~ACharacter();
	
	bool IsMoving();
	void EndMoving();

	void ResetTickCount();
	bool Tact();
	unsigned TickCount();
	unsigned TickMax();

	point NextCell();
	point TargetCell();
	point Cell();

	static void SetBoardInterface(IBoardInterface* b);
	virtual void SetTargetCell(point p) { }

	virtual void Reset(point start, point finish = DEFAULT_POINT) = 0;

	virtual void Draw(sf::RenderWindow& rw) = 0;
	virtual void SetPosition(pointf p) = 0;

};

// ����������� ����� ��������� ���������  ����� ���������
class ACharacterTexture : public ACharacter {
protected:

	sf::Sprite _sprite;
	sf::Texture _tex;

public:

	ACharacterTexture(unsigned speed);
	virtual ~ACharacterTexture();

	void Draw(sf::RenderWindow& rw);
	void SetPosition(pointf p);

};

// ����������� ����� �������� ��������� ���������
class ACharacterAnimated : public ACharacter {
protected:
		
	sf::Texture tex_ps;
	sf::Clock _timer;
	CharacterSprite* _anim;

	void playAnimation(EAnimation type);
	void stopAnimation();

public:

	ACharacterAnimated(unsigned speed);
	virtual ~ACharacterAnimated();

	void Draw(sf::RenderWindow& rw);
	void SetPosition(pointf p);
};