#include "CharatcerSprite.h"
#include "GameWindow.h"
#include "resource.h"


CharacterSprite::CharacterSprite(pointf startPosition, unsigned int frameCount)
{
	// ��������� ����� ����� �������� 
	_frameTime  = 2 * GameWindow::Delta() / frameCount;
	_frameSize = point(FRAME_WIDTH, FRAME_HEIGHT);
	_asprite = new sf::AnimatedSprite(sf::seconds(_frameTime), true, true);
	_asprite->setPosition(startPosition - pointf(_frameSize.x/2,_frameSize.y));
}

CharacterSprite::~CharacterSprite()
{
	delete _asprite;
}

void CharacterSprite::Draw(sf::RenderWindow& rw)
{
	rw.draw(*_asprite);
}

void CharacterSprite::SetPosition(pointf p)
{
	_asprite->setPosition(p - pointf(_frameSize.x/2,_frameSize.y));
}

void CharacterSprite::Update(sf::Time time)
{
	_asprite->update(time);
}

void CharacterSprite::Pause()
{
	_asprite->stop();
}

void CharacterSprite::Play(EAnimation type)
{
	switch (type) {
	case EAnimation::Up: _asprite->play(walkingUp); break;
	case EAnimation::Down: _asprite->play(walkingDown); break;
	case EAnimation::Left: _asprite->play(walkingLeft); break;
	case EAnimation::Right: _asprite->play(walkingRight); break;
	}
}

void CharacterSpriteSheet::loadAnimatedTex(std::string filename)
{
	_tex.loadFromFile(filename);
	walkingUp.setSpriteSheet(_tex);
	walkingUp.addFrame(sf::IntRect( 1*_frameSize.x, 3*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingUp.addFrame(sf::IntRect( 2*_frameSize.x, 3*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingUp.addFrame(sf::IntRect( 1*_frameSize.x, 3*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingUp.addFrame(sf::IntRect( 0*_frameSize.x, 3*_frameSize.y, _frameSize.x, _frameSize.y));

	walkingDown.setSpriteSheet(_tex);
	walkingDown.addFrame(sf::IntRect( 1*_frameSize.x, 0*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingDown.addFrame(sf::IntRect( 2*_frameSize.x, 0*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingDown.addFrame(sf::IntRect( 1*_frameSize.x, 0*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingDown.addFrame(sf::IntRect( 0*_frameSize.x, 0*_frameSize.y, _frameSize.x, _frameSize.y));

	walkingLeft.setSpriteSheet(_tex);
	walkingLeft.addFrame(sf::IntRect( 1*_frameSize.x, 1*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingLeft.addFrame(sf::IntRect( 2*_frameSize.x, 1*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingLeft.addFrame(sf::IntRect( 1*_frameSize.x, 1*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingLeft.addFrame(sf::IntRect( 0*_frameSize.x, 1*_frameSize.y, _frameSize.x, _frameSize.y));

	walkingRight.setSpriteSheet(_tex);
	walkingRight.addFrame(sf::IntRect( 1*_frameSize.x, 2*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingRight.addFrame(sf::IntRect( 2*_frameSize.x, 2*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingRight.addFrame(sf::IntRect( 1*_frameSize.x, 2*_frameSize.y, _frameSize.x, _frameSize.y));
	walkingRight.addFrame(sf::IntRect( 0*_frameSize.x, 2*_frameSize.y, _frameSize.x, _frameSize.y));

	_asprite->setAnimation(walkingDown);

}

CharacterSpriteSheet::CharacterSpriteSheet(pointf startPosition, std::string filename)
	: CharacterSprite(startPosition, ID_FRAME_COUNT)
{ 
	loadAnimatedTex(filename); 
}