#include "Board.h"
#include "PlayerCharacter.h"
#include "EnemyCharacters.h"
#include <time.h> 
#include <iostream>
#include "resource.h"

void Board::Draw(sf::RenderWindow& rw)
{
	Update(_player);
	UpdateArray(_guards, guard);
	UpdateArray(_balls,balls);
	UpdateArray(_bosses,boss);
	if (IsTactEnd()) TactEnd();

	for(int i = 0; i<2*_size; i++)
		_walls[i]->Draw(rw);

	for(int i = 0; i<_size; i++)
		for(int j = 0; j<_size; j++)
			_board[i][j]->Draw(rw);
	
	_player->Draw(rw);
	for(int i=0;i<balls;i++) _balls[i]->Draw(rw);
	for(int i=0;i<guard;i++) _guards[i]->Draw(rw);
	for(int i=0;i<boss;i++) _bosses[i]->Draw(rw);
	DrawGUI(rw);
}

void Board::MouseMove(int x, int y)
{
	point n = getCell(pointf(x,y));

	if (n.x > -1  && n.y > -1) 
		if (_cur_cell.x>-1 && _cur_cell.y>-1) 
		{ 
			_board[_cur_cell.x][_cur_cell.y]->IsLight(false); 
			_board[n.x][n.y]->IsLight(true); 
			_cur_cell = n; 
		} else {
			_board[n.x][n.y]->IsLight(true); 
			_cur_cell = n; 
		}
	else 
		if (_cur_cell.x>-1 && _cur_cell.y>-1) 
		{
			_board[_cur_cell.x][_cur_cell.y]->IsLight(false); 
			_cur_cell = n; 
		}

}

void Board::MouseClickL(int x, int y)
{
	point n = getCell(pointf(x,y));
	if (n.x > -1 && n.y > -1 && _board[n.x][n.y]->IsFree()) _target = n;
	
}

void Board::MouseClickR(int x, int y)
{
	point n = getCell(pointf(x,y));
	
	if (n.x > -1 && n.y > -1) 
		if (_board[n.x][n.y]->IsFree()) _board[n.x][n.y]->IsFree(false);
		else _board[n.x][n.y]->IsFree(true);
}

void Board::reset_all()
{
	Restart();
	srand (time(NULL));
	_cur_cell = point(-1,-1);
	_target = point(0,0);
	_cur_bonus = EBonus::None;

	for(int i = 0; i<2*_size; i++)
		_walls[i]->HasHole(false);
	for(int i = 0; i<_size; i++)
		for(int j = 0; j<_size; j++)
			if (!_board[i][j]->IsGrave())  
				_board[i][j]->SetBonus(EBonus::None);

	for(ushort i=0;i<rand()%3; i++)
	{
		point p = point((rand() % (_size-1) + 1), (rand() % (_size-1) + 1));
		switch(rand()%4) {
		case 0: 
			_board[p.x][p.y]->SetBonus(EBonus::Bonus_1); 
			break;
		case 1: 
			_board[p.x][p.y]->SetBonus(EBonus::Bonus_2);
			break;
		case 2: 
			_board[p.x][p.y]->SetBonus(EBonus::Bonus_3);
			break;
		case 3: 
			_board[p.x][p.y]->SetBonus(EBonus::Health);
			break;
		}

	}

	for (ushort i=0;i<portals;i++)
	{
		point p;
		do {
			p = point((rand() % (_size-1) + 1), (rand() % (_size-1) + 1));
		} while (_board[p.x][p.y]->IsPortal() || !_board[p.x][p.y]->IsFree());
		_board[p.x][p.y]->SetBonus(EBonus::Portal);
	}


	_player->Reset(DEFAULT_POINT);
	_target = DEFAULT_POINT;
	_ironMan = false;

	for(ushort i=0;i<guard;i++)
	{
		bool ver = (i % 2 != 0);
		short j = rand() % _size;
		point a1,a2;
		do {
			a1 = ver ? point(j,rand()%_size) : point(rand()%_size,j);
			a2 = ver ? point(j,rand()%_size) : point(rand()%_size,j);
		} while(a1==a2 || a1==point(0,0) || a2==point(0,0));
		_guards[i]->Reset(a1,a2);
	}

	for(ushort i=0;i<balls;i++)
	{
		bool ver = (i % 2 == 0);
		short j = rand() % (_size-1);
		if (ver) _walls[j+1]->HasHole(true);
		else _walls[_size + j + 1]->HasHole(true);
		point a1 = ver ? point(0,j+1) : point(j+1,_size-1);
		point a2 = ver ? point(_size-1,j+1) : point(j+1,0);
		_balls[i]->Reset(a1,a2);
	}

}

Board::Board(uint size)
{
	// ������ ����� �� ������
	_size = size;
	Cell::Width(GameWindow::Width() / _size);
	Cell::Height(GameWindow::Height() / (_size + 1));
	_board = new Cell**[_size];
	_board[0] = new Cell*[_size];
	_board[0][0] = new Cell(pointf(Cell::Width()/2, GameWindow::Height() / 2 + Cell::Height() / 2));
	for(int i=1;i<_size;i++) _board[0][i] = 
		new Cell(_board[0][0]->Center() + pointf(i * Cell::Width()/2, - i * Cell::Height()/2));
	for(int j=1;j<_size;j++) 
	{
		_board[j] = new Cell*[_size]; 
		_board[j][0] = new Cell(_board[j-1][0]->Center() + pointf(Cell::Width()/2, Cell::Height()/2));
		for(int i=1;i<_size;i++) _board[j][i] = 
			new Cell(_board[j][0]->Center() + pointf(i * Cell::Width()/2, - i * Cell::Height()/2));
	}

	// ������ �����
	_walls = new Wall*[2*_size];
	for(int i=0;i<_size;i++) _walls[i] = new Wall(_board[0][i]->Center(), false);
	for(int i=0;i<_size;i++) _walls[_size + i] = new Wall(_board[i][_size - 1]->Center(), true);

	// ������ ����������
	ACharacter::SetBoardInterface(this);
	_player = new PlayerCharacter(ID_PLAYER_SPEED);
	_player->Reset(point(0,0));

	_guards = new ACharacter*[guard_max];
	for(int i=0;i<guard_max;i++) _guards[i] = new GuardCharacter(ID_GUARD_SPEED);

	_balls = new ACharacter*[balls_max];
	for(int i=0;i<balls_max;i++) _balls[i] = new Fireball(ID_BALL_SPEED);

	_bosses = new ACharacter*[boss_max];
	for(int i=0;i<boss_max;i++) _bosses[i] = new BossCharacter(ID_BOSS_SPEED);
	
	PlayMusic(EMusic::Backgraund);

	reset_all();
	
}

Board::~Board()
{
	for(int i = 0; i<2*_size; i++)
		delete _walls[i];
	delete[] _walls;

	for(int i = 0; i<_size; i++)
		for(int j = 0; j<_size; j++)
			delete _board[i][j];
	for(int i = 0; i<_size; i++)
		delete[] _board[i];
	delete[] _board;

	for(int i = 0; i<balls_max; i++)
		delete[] _balls[i];
	delete[] _balls;

	for(int i = 0; i<guard_max; i++)
		delete[] _guards[i];
	delete[] _guards;

	for(int i = 0; i<boss_max; i++)
		delete[] _bosses[i];
	delete[] _bosses;

	delete _player;
}

void Board::LoadTexture(const sf::Texture& texture, ushort texType)
{
	switch(texType) {
	case ETex::Floor:
		for(int i = 0; i<_size; i++)
			for(int j = 0; j<_size; j++)
				_board[i][j]->LoadTexture(texture, texType);
		break;
	case ETex::WallLeft:
		for(int i = 0; i<_size; i++)
			_walls[i]->LoadTexture(texture, texType);
		break;
	case ETex::WallRight:
		for(int i = _size; i<2*_size; i++)
			_walls[i]->LoadTexture(texture, texType);
		break;
	}

}

point Board::getCell(pointf p)
{
	if (Cell::InCellArea(p,pointf(float(GameWindow::Width())/2,float(GameWindow::Height())/2 + float(Cell::Height())/2),
		GameWindow::Width(), GameWindow::Height() - Cell::Height()))
	{
		if (_cur_cell.x > -1 && _cur_cell.y > -1 && _board[_cur_cell.x][_cur_cell.y]->InCell(p))
		{
			return _cur_cell;
		}
		else
		{
			for(int i=0;i<_size;i++)
				for(int j=0;j<_size;j++)
					if (_board[i][j]->InCell(p)) return point(i,j);
			return point(-1,-1);
		}
	}
	else return point(-1,-1);
}

pointf Board::CCenter(point cell)
{
	return _board[cell.x][cell.y]->Center();
}

bool Board::InArea(point cell)
{
	return (cell.x >= 0 && cell.y >= 0 && cell.x < _size && cell.y < _size);
}

bool Board::IsCFree(point cell)
{
	return _board[cell.x][cell.y]->IsFree();
}

void Board::TactEnd()
{
	EBonus tmp = _board[_player->Cell().x][_player->Cell().y]->GetBonus();
	if (tmp != EBonus::None)
	{
		
		switch (tmp)
		{
		case EBonus::Portal: 
			if (OnPortal()) { GameOver(true); return; }
			break;
		case EBonus::Health:
			_health++;
			break;
		case EBonus::Grave:
			_player->ResetSheet(false);
			_player->SetTargetCell(_target);
			_player->SetPosition(CCenter(_player->Cell()));
			_ironMan = true;
			PlayMusic(EMusic::IronMan);
			_timer.restart();
			break;
		case EBonus::Bonus_1:
		case EBonus::Bonus_2:
		case EBonus::Bonus_3:
			PlayMusic(EMusic::TakeBonus);
			_cur_bonus = tmp;
			break; 
		}
	}

	if (_ironMan && _timer.getElapsedTime() > sf::seconds(IRON_MAN_TIME))
	{
		_player->ResetSheet(true);
		_player->SetTargetCell(_target);
		_player->SetPosition(CCenter(_player->Cell()));
		_ironMan = false;

	}
	for(int i=0;i<guard;i++)
		if (_guards[i]->Cell() == _player->Cell() && _cur_bonus != EBonus::Bonus_1 && !_ironMan) _health--;
	for(int i=0;i<balls;i++)
		if (_balls[i]->Cell() == _player->Cell() && _cur_bonus != EBonus::Bonus_2 && !_ironMan) _health--;
	for(int i=0;i<boss;i++)
		if (_bosses[i]->Cell() == _player->Cell() && _cur_bonus != EBonus::Bonus_3 && !_ironMan) _health--;

	if (level>ID_BOSS_START_LEVEL && rand() % ID_BOSS_CHANCE == 1 && boss+1<=boss_max)  
	{ 
		PlayMusic(EMusic::Boss); 
		boss++;
		_bosses[boss-1]->Reset(point((rand() % (_size-1) + 1), (rand() % (_size-1) + 1)));
	} 

	if (_health <= 0) 
	{ 
		_board[_player->Cell().x][_player->Cell().y]->SetBonus(EBonus::Grave);  
		GameOver(false); 
		return; 
	}
	else if (_target != _player->TargetCell() && _player->TickCount() == 0) 
	{
		_player->SetTargetCell(_target); 
	}
}

point Board::PlayerCell()
{
	return _player->Cell();
}

void Board::TryHold(point cell)
{
	srand(time(NULL));
	if (rand() % 20 == 0) _board[cell.x][cell.y]->IsFree(false);
}

void Board::GameOver(bool isWin)
{
	if (isWin) { Win(); reset_all(); }
	else { Lose(); reset_all(); }
}




