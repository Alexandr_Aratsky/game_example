#include "GameWindow.h"
#include "resource.h"

int GameWindow::_width = SCREEN_WIDTH;
int GameWindow::_height = SCREEN_HEIGHT;
float GameWindow::_delta_time = DELTA_TIME;

int GameWindow::Width()
{
	return _width;
}

void GameWindow::Width(int val)
{
	_width = val;
}

int GameWindow::Height()
{
	return _height;
}

void GameWindow::Height(int val)
{
	_height = val;
}

float GameWindow::Delta()
{
	return _delta_time;
}

sf::Time GameWindow::DeltaTime()
{
	return sf::seconds(_delta_time);
}

void GameWindow::Delta_time(float val)
{
	_delta_time = val;
}
