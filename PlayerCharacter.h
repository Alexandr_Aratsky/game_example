#pragma once

#include "ACharacter.h"
#include <list>

class PlayerCharacter: public ACharacterAnimated {
private:

	bool _lost;
	CharacterSprite* _anim1;
	CharacterSprite* _anim2;

	float distance(pointf a, pointf b);
	float distance(point a, point b);

	static const int near_count;
	static const point *nears;
	static EAnimation getAnimationType(int i);

	std::list<point> _way;
	std::list<point> _bad_way;

protected:

	void next_step();
	void end_moving_handler();

public:
	PlayerCharacter(unsigned ticks);
	virtual ~PlayerCharacter();
	void SetTargetCell(point cell);
	void ResetSheet(bool isFirst = true);
	virtual void Reset(point start, point finish = DEFAULT_POINT);
};
