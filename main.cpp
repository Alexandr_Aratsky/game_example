#include <SFML/Graphics.hpp>
#include "Cell.h"
#include "Board.h"
#include "CharatcerSprite.h"
#include "resource.h"



int main()
{
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), 
		"Game example", sf::Style::Close | sf::Style::Titlebar, settings);

	Board b(BOARD_SIZE);

	sf::Texture tex_fl;
	tex_fl.loadFromFile(ID_FLOOR);
	b.LoadTexture(tex_fl,ETex::Floor);

	sf::Texture tex_wl;
	tex_wl.loadFromFile(ID_WALL_LEFT);
	b.LoadTexture(tex_wl,ETex::WallLeft);

	sf::Texture tex_wr;
	tex_wr.loadFromFile(ID_WALL_RIGHT);
	b.LoadTexture(tex_wr,ETex::WallRight);

	sf::Clock frameClock;
	while (window.isOpen())
	{
		sf::Time frameTime = frameClock.restart();

		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			if (event.type == sf::Event::MouseMoved) 
				b.MouseMove(event.mouseMove.x,event.mouseMove.y);
			if (event.type == sf::Event::MouseButtonPressed) 
			{
				if (event.mouseButton.button == sf::Mouse::Right)
					b.MouseClickR(event.mouseButton.x,event.mouseButton.y);
				if (event.mouseButton.button == sf::Mouse::Left)
					b.MouseClickL(event.mouseButton.x,event.mouseButton.y);
			}
		}


		window.clear();
		b.Draw(window);
		window.display();
	}

	return 0;
}