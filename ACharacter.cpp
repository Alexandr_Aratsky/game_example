#include "ACharacter.h"
#include "CharatcerSprite.h"
#include "GameWindow.h"

// ACharacter

IBoardInterface* ACharacter::_b = NULL;

ACharacter::ACharacter(unsigned speedTick) : _tickMax(speedTick)
{
	_target_cell = _next_cell = _current_cell = DEFAULT_POINT;
	_isMoving = false;
	_tickCount = 0;
}

ACharacter::~ACharacter()
{ }

bool ACharacter::IsMoving()
{ return _isMoving; }

void ACharacter::EndMoving()
{
	_current_cell = _next_cell;
	
	// ����� ����� ���������� ������������, ��� �� 
	// ������������ ����� �������� � ������������ � �� �������
	end_moving_handler();
	
	// ��������� ����� ���������� ���� ���� �� �������� ����
	if (_target_cell != _current_cell) next_step();
}

void ACharacter::ResetTickCount()
{ _tickCount = 0; }

bool ACharacter::Tact()
{ return (++_tickCount < _tickMax); }

unsigned ACharacter::TickCount()
{ return _tickCount; }

unsigned ACharacter::TickMax()
{ return _tickMax; }

point ACharacter::NextCell()
{ return _next_cell; }

point ACharacter::Cell()
{ return _current_cell; }

point ACharacter::TargetCell()
{ return _target_cell; }

point ACharacter::v_sign(point a)
{
	return point(
		((a.x == 0) ? 0 : ((a.x < 0) ? -1 : 1)),
		((a.y == 0) ? 0 : ((a.y < 0) ? -1 : 1)));
}

void ACharacter::SetBoardInterface(IBoardInterface* b)
{ _b = b; }


// ACharacterAnimated

ACharacterAnimated::ACharacterAnimated(unsigned speed) : ACharacter(speed)
{ }

ACharacterAnimated::~ACharacterAnimated()
{ delete _anim; }

void ACharacterAnimated::playAnimation(EAnimation type)
{ _anim->Play(type); }

void ACharacterAnimated::stopAnimation()
{ _anim->Pause(); }

void ACharacterAnimated::Draw(sf::RenderWindow& rw)
{
	_anim->Update(_timer.restart());
	_anim->Draw(rw);
}

void ACharacterAnimated::SetPosition(pointf p)
{ _anim->SetPosition(p); }



// ACharacterTexture

ACharacterTexture::ACharacterTexture(unsigned speed) : ACharacter(speed)
{ }

ACharacterTexture::~ACharacterTexture()
{ }

void ACharacterTexture::Draw(sf::RenderWindow& rw)
{ rw.draw(_sprite); }

void ACharacterTexture::SetPosition(pointf p)
{
	pointf size = (pointf) _tex.getSize();
	_sprite.setPosition(p.x - size.x/2, p.y - size.y/2);
}

