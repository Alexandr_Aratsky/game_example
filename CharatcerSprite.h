#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Animation.hpp>
#include <string>
#include "Interfaces.h"

class CharacterSprite : public IDrowable {
private:

	float _frameTime;

protected:

	point _frameSize;

	sf::AnimatedSprite* _asprite;
	sf::Animation walkingDown;
	sf::Animation walkingLeft;
	sf::Animation walkingRight;
	sf::Animation walkingUp;
	sf::Animation hold;

	CharacterSprite(pointf startPosition, unsigned int frameCount);
	virtual void loadAnimatedTex(std::string filename) = 0;

public:

	virtual ~CharacterSprite();

	void Play(EAnimation type);
	void Pause();

	void SetPosition(pointf p);
	void Update(sf::Time time);

	virtual void Draw(sf::RenderWindow& rw);
};

class CharacterSpriteSheet : public CharacterSprite {
protected:
	sf::Texture _tex;
	virtual void loadAnimatedTex(std::string filename);

public:

	CharacterSpriteSheet(pointf startPosition, std::string filename);
};