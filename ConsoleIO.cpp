#include "ConsoleIO.h"
#include <iostream>

using namespace std;

void AConsoleIO::console_println(std::string msg)
{
	cout<<msg<<endl;
}

std::string AConsoleIO::i2s(int i)
{
	ss.str((""));
	ss<<i;
	return ss.str();
}

std::string AConsoleIO::f2s(float f)
{
	ss.str((""));
	ss<<f;
	return ss.str();
}
