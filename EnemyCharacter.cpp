#include "EnemyCharacter.h"
#include "resource.h"

void GuardCharacterSprite::loadAnimatedTex()
{
	_tex.loadFromFile(ID_GUARD);
	point size(32,32);
	walkingUp.setSpriteSheet(_tex);
	walkingUp.addFrame(sf::IntRect( 1*size.x, 3*size.y, size.x, size.y));
	walkingUp.addFrame(sf::IntRect( 2*size.x, 3*size.y, size.x, size.y));
	walkingUp.addFrame(sf::IntRect( 1*size.x, 3*size.y, size.x, size.y));
	walkingUp.addFrame(sf::IntRect( 0*size.x, 3*size.y, size.x, size.y));

	walkingDown.setSpriteSheet(_tex);
	walkingDown.addFrame(sf::IntRect( 1*size.x, 0*size.y, size.x, size.y));
	walkingDown.addFrame(sf::IntRect( 2*size.x, 0*size.y, size.x, size.y));
	walkingDown.addFrame(sf::IntRect( 1*size.x, 0*size.y, size.x, size.y));
	walkingDown.addFrame(sf::IntRect( 0*size.x, 0*size.y, size.x, size.y));

	walkingLeft.setSpriteSheet(_tex);
	walkingLeft.addFrame(sf::IntRect( 1*size.x, 1*size.y, size.x, size.y));
	walkingLeft.addFrame(sf::IntRect( 2*size.x, 1*size.y, size.x, size.y));
	walkingLeft.addFrame(sf::IntRect( 1*size.x, 1*size.y, size.x, size.y));
	walkingLeft.addFrame(sf::IntRect( 0*size.x, 1*size.y, size.x, size.y));

	walkingRight.setSpriteSheet(_tex);
	walkingRight.addFrame(sf::IntRect( 1*size.x, 2*size.y, size.x, size.y));
	walkingRight.addFrame(sf::IntRect( 2*size.x, 2*size.y, size.x, size.y));
	walkingRight.addFrame(sf::IntRect( 1*size.x, 2*size.y, size.x, size.y));
	walkingRight.addFrame(sf::IntRect( 0*size.x, 2*size.y, size.x, size.y));

	_asprite->setAnimation(walkingDown);
	_asprite->pause();
}

GuardCharacterSprite::GuardCharacterSprite(pointf startPosition, unsigned int frameCount)
	: CharacterSprite(startPosition, frameCount)
{ 
	loadAnimatedTex(); 
}


void GuardCharacter::next_step()
{
	point p = v_sign(_target_cell - _current_cell);
	_next_cell = _current_cell + p; 

	if (p.x > 0 && p.y >= 0) playAnimation(EAnimation::Right);
	else if (p.x >= 0 && p.y < 0) playAnimation(EAnimation::Down);
	else if (p.x < 0 && p.y <= 0) playAnimation(EAnimation::Left);
	else if (p.x <= 0 && p.y > 0) playAnimation(EAnimation::Up);
	
}

GuardCharacter::GuardCharacter(unsigned ticks, IBoardInterface& b, point a1, point a2)
	: ACharacterAnimated(ticks,a1), _a1(a1), _a2(a2), _b(&b)
{
	_anim = new GuardCharacterSprite(pointf(_b->CCenter(_current_cell)),ID_FRAME_COUNT);
	_isMoving = true;
	_target_cell = _a2;
	next_step();
}

point GuardCharacter::v_sign(point a)
{
	return point(
		((a.x == 0) ? 0 : ((a.x < 0) ? -1 : 1)),
		((a.y == 0) ? 0 : ((a.y < 0) ? -1 : 1)));
}

void GuardCharacter::end_moving_handler()
{
	if (_current_cell == _a1) {  _target_cell = _a2; _isMoving = true; }
	if (_current_cell == _a2) {  _target_cell = _a1; _isMoving = true; }
}



