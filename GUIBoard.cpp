#include "GUIBoard.h"
#include "resource.h"
#include "GameWindow.h"
#include <iostream>


BoardInfo::BoardInfo()
	: boss_max(ID_BOSS_MAX), guard_max(ID_GUARD_MAX), balls_max(ID_FIREBALLS_MAX), portal_max(ID_PORTALS_MAX)
{
	level = 1;
	win_count = 0;
	lose_count = 0;
	guard = 1;
	balls = 0;
	boss = 0;
	portals = 1;
	portal_count = 0;
	_health = 1;
}

void GUIBoard::Restart()
{
	if (guard+1 <= guard_max && rand() % 2 == 0) guard++;
	if (balls+1 <= balls_max && rand() % 2 == 0) balls++;
	if (portals+1 <= portal_max && rand() % 2 == 0) portals++;
	boss = 0;
	portal_count = 0;
	_health = 1;
}

bool GUIBoard::OnPortal()
{
	sound_portal.play();
	return ++portal_count >= portals;
}

void GUIBoard::PlayMusic(EMusic musicType)
{
	switch(musicType) {
	case EMusic::Backgraund: sound_backgroud.play(); break;
	case EMusic::Boss: sound_boss.play(); break;
	case EMusic::TakeBonus: sound_bonus.play(); break;
	case EMusic::IronMan: sound_ironman.play(); break;
	}
}

std::string bonusType(EBonus t)
{
	switch(t) {
	case EBonus::Bonus_1: return "Knife";
	case EBonus::Bonus_2: return "Shield";
	case EBonus::Bonus_3: return "Mask";
	default: return "---";
	}

}

void GUIBoard::DrawGUI(sf::RenderWindow& rw)
{
	text_left.setString("Level: " + i2s(level) + "\nWins: " + i2s(win_count) + "\nLoses: " + i2s(lose_count));
	text_right.setString("Portals: " + i2s(portal_count) + "\nHealth: " + i2s(_health) + "\nBonus: " + bonusType(_cur_bonus));
	rw.draw(text_left);
	rw.draw(text_right);
}

void GUIBoard::Win()
{
	win_count++;
	level++;
	sound_win.play();
}

GUIBoard::GUIBoard()
{

	font.loadFromFile(ID_FONT);
	
	text_left.setFont(font);
	text_left.setCharacterSize(FONT_SIZE);
	text_left.setPosition(FONT_SIZE * 0.5f,FONT_SIZE * 1.2f);
	text_left.setColor(sf::Color::White);
	text_left.setStyle(sf::Text::Bold);
	
	text_right.setFont(font);
	text_right.setCharacterSize(FONT_SIZE);
	text_right.setPosition(GameWindow::Width() * 0.8f,FONT_SIZE * 1.2f);
	text_right.setColor(sf::Color::White);
	text_right.setStyle(sf::Text::Bold);

	buffer_backgroud.loadFromFile(ID_MUSIC_BACKGRAUND);
	sound_backgroud.setBuffer(buffer_backgroud);
	sound_backgroud.setLoop(true);

	buffer_win.loadFromFile(ID_SOUND_WIN);
	sound_win.setBuffer(buffer_win);

	buffer_lose.loadFromFile(ID_SOUND_LOSE);
	sound_lose.setBuffer(buffer_lose);

	buffer_boss.loadFromFile(ID_SOUND_BOSS);
	sound_boss.setBuffer(buffer_boss);

	buffer_portal.loadFromFile(ID_SOUND_PORTAL);
	sound_portal.setBuffer(buffer_portal);
	sound_portal.setVolume(50);

	buffer_bonus.loadFromFile(ID_SOUND_BONUS);
	sound_bonus.setBuffer(buffer_bonus);

	buffer_ironman.loadFromFile(ID_SOUND_IRONMAN);
	sound_ironman.setBuffer(buffer_ironman);

}

void GUIBoard::Lose()
{
	lose_count++;
	level++;
	sound_lose.play();
}
