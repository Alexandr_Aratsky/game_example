#include "CharacterController.h"
#include "GameWindow.h"
#include <SFML//System/Clock.hpp>

CharacterController::CharacterController()
{
		_timer.restart();
}

void CharacterController::ConotrollerRestart()
{
	_timer.restart();
}

//void CharacterController::Update()
//{
//	//if (_ib == NULL) throw new std::logic_error();
//	// ���������� ������ ������� �����
//	if (_timer.getElapsedTime() < GameWindow::DeltaTime()) 
//	{
//		// ���� ������� �� ����������
//		// ���������� ���� ����������
//		std::list<std::shared_ptr<ICharacter>>::iterator it;
//		for(it = _ch_list.begin(); it != _ch_list.end(); it++)
//			if ((*it)->IsMoving()) 
//			{
//				// ���������� ��������� �� ������� ������ �� ����, 
//				// ��� ��� �������� - ��� ����� ������, ������� ��� ��������� 
//				// ��� ����������� �� ������� ������ � ��������
//				float delta = _timer.getElapsedTime() / GameWindow::DeltaTime();
//				pointf cur = _ib->CCenter((*it)->Cell());
//				pointf next = _ib->CCenter((*it)->NextCell());
//				(*it)->SetPosition(cur + (((*it)->TickCount() + delta)/(*it)->TickMax())*(next-cur));
//			}
//	} else {
//		// ���� ������� �������� � ����� ������� ��� ���������� ������� ����� �� ����
//		_timer.restart();
//		std::list<std::shared_ptr<ICharacter>>::iterator it;
//		for(it = _ch_list.begin(); it != _ch_list.end(); it++)
//			if (!(*it)->Tact()) 
//			{ 
//				(*it)->EndMoving();
//				(*it)->SetPosition(_ib->CCenter((*it)->Cell()));
//				(*it)->ResetTickCount();
//			}
//			// ������������� ����� � ���������� �����
//			if (_board_notify) _ib->TactEnd();
//	}
//}


void CharacterController::Update(ACharacter* ch)
{
	if (_timer.getElapsedTime() < GameWindow::DeltaTime()) update_position(ch);
	else was_moved(ch);
}


void CharacterController::update_position(ACharacter* ch)
{
	if (ch->IsMoving()) 
	{
		float delta = _timer.getElapsedTime() / GameWindow::DeltaTime();
		pointf cur = CCenter(ch->Cell());
		pointf next = CCenter(ch->NextCell());
		ch->SetPosition(cur + ((ch->TickCount() + delta)/ch->TickMax())*(next-cur));
	}
}

void CharacterController::was_moved(ACharacter* ch)
{
	if (!ch->Tact()) 
	{ 
		ch->EndMoving();
		ch->SetPosition(CCenter(ch->Cell()));
		ch->ResetTickCount();
	}
}

bool CharacterController::IsTactEnd()
{
	if (_timer.getElapsedTime() > GameWindow::DeltaTime()) {
		_timer.restart();
		return true;
	} else return false;
}

void CharacterController::UpdateArray(ACharacter** ch_arr, int n)
{
	if (_timer.getElapsedTime() < GameWindow::DeltaTime()) 
	{
		for(int i = 0; i < n; i++)
			update_position(ch_arr[i]);
	}
	else 
	{
		for(int i = 0; i < n; i++)
			was_moved(ch_arr[i]);
	}
}
