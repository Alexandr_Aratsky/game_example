#include "Cell.h"
#include "resource.h"
#include <SFML/Graphics/Color.hpp>

#include <math.h>
#define PI 3.14159265



// Cell

float Cell::_height = 1;
float Cell::_width = 1;

float Cell::Width()
{
	return _width;
}

void Cell::Width(float val)
{
	_width = val;
}

float Cell::Height()
{
	return _height;
}

void Cell::Height(float val)
{
	_height = val;
}

bool Cell::IsFree() const
{
	return _isFree;
}

void Cell::IsFree(bool val)
{
	_isFree = val;
	config_color();
}

Cell::Cell(pointf center)
{
	_center = center;
	_isFree = true;
	_isLight = false;
	_shape = cell_shape();
	config_shape();
	config_color();
	config_bonus(EBonus::None);
}


Cell::~Cell()
{ }

pointf Cell::Center() const
{
	return _center;
}

void Cell::Center(pointf val)
{
	_center = val;
	config_shape();
}

bool Cell::InCell(pointf p)
{
	return ((_width*p.y - _width*_center.y) + (_height*p.x - _height*_center.x) < _width*_height/2 
		&& (_width*p.y - _width*_center.y) + (_height*p.x - _height*_center.x) > - _width*_height/2 
		&& (_width*p.y - _width*_center.y) - (_height*p.x - _height*_center.x) < _height*_width/2 
		&& (_width*p.y - _width*_center.y) - (_height*p.x - _height*_center.x) > - _width*_height/2);
}

void Cell::config_shape()
{
	_shape.setPointCount(4);
	_shape.setPoint(0, pointf(_center.x, _center.y + COEF_TICK * _height/2));
	_shape.setPoint(1, pointf(_center.x + COEF_TICK * _width/2, _center.y));
	_shape.setPoint(2, pointf(_center.x, _center.y - COEF_TICK * _height/2));
	_shape.setPoint(3, pointf(_center.x - COEF_TICK * _width/2, _center.y));
}

void Cell::config_color()
{
	if (_isFree) {
		if (_isLight) { _shape.setFillColor(sf::Color::Transparent); _shape.setOutlineThickness(THICKNESS); _shape.setOutlineColor(CLIGHTGREEN); }
		else { _shape.setFillColor(sf::Color::Transparent); _shape.setOutlineThickness(0); }
	} else {
		if (_isLight) { _shape.setFillColor(CDARKGREEN); _shape.setOutlineThickness(THICKNESS); _shape.setOutlineColor(CLIGHTGREEN); }
		else { _shape.setFillColor(CDARKGREEN); _shape.setOutlineThickness(0); }
	}
}

void Cell::Draw(sf::RenderWindow& rw)
{
	rw.draw(_sprite);
	_bonus.Draw(rw);
	 rw.draw(_shape); 
}

bool Cell::IsLight() const
{
	return _isLight;
}

void Cell::IsLight(bool val)
{
	_isLight = val;
	config_color();
}

void Cell::LoadTexture(const sf::Texture& texture, ushort texType)
{	
	_sprite.setTexture(texture);
	_sprite.setPosition(_center.x - _width/2, _center.y - _height/2);
	_sprite.setScale(_width/texture.getSize().x, _height/texture.getSize().y);
}

bool Cell::InCellArea(pointf p, pointf c, float w, float h)
{
	return ((w*p.y - w*c.y) + (h*p.x - h*c.x) < w*h/2 
		&& (w*p.y - w*c.y) + (h*p.x - h*c.x) > - w*h/2 
		&& (w*p.y - w*c.y) - (h*p.x - h*c.x) < w*h/2 
		&& (w*p.y - w*c.y) - (h*p.x - h*c.x) > - w*h/2);
}

void Cell::config_bonus(EBonus _bonusType)
{
	_bonus.Type(_bonusType);
	_bonus.SetPosition(_center);
}

EBonus Cell::GetBonus()
{
	EBonus tmp = _bonus.Type();
	_bonus.Type(EBonus::None);
	return tmp;
}

void Cell::SetBonus(EBonus bonusType)
{
	config_bonus(bonusType);
}

bool Cell::IsPortal()
{
	return (EBonus::Portal == _bonus.Type());
}

bool Cell::IsGrave()
{
	return (EBonus::Grave == _bonus.Type());
}


// CellObject

point CellObject::_size = point(-1,-1);

void CellObject::Draw(sf::RenderWindow& rw)
{
	if (_type != EBonus::None) rw.draw(_obj);
}

CellObject::CellObject()
{
	if (_size.x < 0 && _size.y < 0) LoadTextures();
	_type = EBonus::None;	
}

EBonus CellObject::Type() const
{
	return _type;
}

void CellObject::Type(EBonus val)
{
	_type = val;
	switch (_type) {
		case EBonus::Portal: 
			_obj.setTexture(_portal); 
			break;
		case EBonus::Bonus_1: 
			_obj.setTexture(_bonus_1); 
			break;
		case EBonus::Bonus_2: 
			_obj.setTexture(_bonus_2); 
			break;
		case EBonus::Bonus_3: 
			_obj.setTexture(_bonus_3); 
			break;
		case EBonus::Health: 
			_obj.setTexture(_health); 
			break;
		case EBonus::Grave: 
			_obj.setTexture(_grave); 
			break;
		default: break;
	}
}

void CellObject::SetPosition(pointf p)
{
	_obj.setPosition(p - pointf(_size.x/2, _size.y/2));
}

void CellObject::LoadTextures()
{
	_bonus_1.loadFromFile(ID_BONUS_1);
	_bonus_2.loadFromFile(ID_BONUS_2);
	_bonus_3.loadFromFile(ID_BONUS_3);
	_portal.loadFromFile(ID_PORTAL);
	_health.loadFromFile(ID_HEALTH);
	_grave.loadFromFile(ID_GRAVE);
	_size = point(32,32);
}

sf::Texture CellObject::_health = sf::Texture();

sf::Texture CellObject::_portal = sf::Texture();

sf::Texture CellObject::_bonus_3 = sf::Texture();

sf::Texture CellObject::_bonus_2 = sf::Texture();

sf::Texture CellObject::_bonus_1 = sf::Texture();

sf::Texture CellObject::_grave = sf::Texture();