#pragma once

#include <string>
#include <sstream>

class AConsoleIO {
private:
	std::stringstream ss;
protected:
	void console_println(std::string msg);
	std::string i2s(int i);
	std::string f2s(float f);

};