#include "EnemyCharacters.h"
#include "resource.h"


// GuardCharacter

GuardCharacter::GuardCharacter(unsigned ticks) : ACharacterAnimated(ticks)
{
	_anim = new CharacterSpriteSheet(DEFAULT_POINTF,ID_GUARD);
	_a1 = _a2 = DEFAULT_POINT;
	_from1to2 = true;
}

void GuardCharacter::next_step()
{
	point p = ACharacter::v_sign(_target_cell - _current_cell);
	_next_cell = _current_cell + p; 

	if (p.x > 0 && p.y >= 0) playAnimation(EAnimation::Right);
	else if (p.x >= 0 && p.y < 0) playAnimation(EAnimation::Down);
	else if (p.x < 0 && p.y <= 0) playAnimation(EAnimation::Left);
	else if (p.x <= 0 && p.y > 0) playAnimation(EAnimation::Up);
}

void GuardCharacter::end_moving_handler()
{
	if (_current_cell == _a1) {  _target_cell = _a2; _isMoving = true; }
	if (_current_cell == _a2) {  _target_cell = _a1; _isMoving = true; }
}

GuardCharacter::~GuardCharacter() { }

void GuardCharacter::Reset(point start, point finish /*= DEFAULT_POINT*/)
{
	_current_cell = _a1 = start;
	_target_cell = _a2 = finish;
	_isMoving = _from1to2 = true;
	SetPosition(_b->CCenter(_current_cell));
	next_step();
}


// BossCharacter

BossCharacter::BossCharacter(unsigned ticks): ACharacterAnimated(ticks)
{
	_anim = new CharacterSpriteSheet(DEFAULT_POINTF,ID_BOSS);
	_isMoving = true;
}

BossCharacter::~BossCharacter()
{ }

void BossCharacter::next_step()
{
	point p = ACharacter::v_sign(_target_cell - _current_cell);
	_next_cell = _current_cell + p; 

	if (p.x > 0 && p.y >= 0) playAnimation(EAnimation::Right);
	else if (p.x >= 0 && p.y < 0) playAnimation(EAnimation::Down);
	else if (p.x < 0 && p.y <= 0) playAnimation(EAnimation::Left);
	else if (p.x <= 0 && p.y > 0) playAnimation(EAnimation::Up);
}

void BossCharacter::end_moving_handler()
{
	_target_cell = _b->PlayerCell();
	_isMoving = true;
	_b->TryHold(_current_cell);
}

void BossCharacter::Reset(point start, point finish /*= DEFAULT_POINT*/)
{
	_current_cell = start;
	_target_cell = _b->PlayerCell();
	_isMoving = true;
	SetPosition(_b->CCenter(_current_cell));
	next_step();
}


Fireball::Fireball(unsigned ticks) : ACharacterTexture(ticks)
{
	_tex.loadFromFile(ID_FIREBALL);
	_sprite.setTexture(_tex);
	_a1 = _a2 = DEFAULT_POINT;
	_from1to2 = true;
}

void Fireball::end_moving_handler()
{
	if (_current_cell == _a1) {  _target_cell = _a2; _isMoving = true; }
	if (_current_cell == _a2) { _current_cell = _a1; _isMoving = true; }
}

void Fireball::next_step()
{
	point p = ACharacter::v_sign(_target_cell - _current_cell);
	_next_cell = _current_cell + p; 
}

void Fireball::Reset(point start, point finish /*= DEFAULT_POINT*/)
{
	_current_cell = _a1 = start;
	_target_cell = _a2 = finish;
	_isMoving = _from1to2 = true;
	SetPosition(_b->CCenter(_current_cell));
	next_step();
}

