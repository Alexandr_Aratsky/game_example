#include "PlayerCharacter.h"
#include <math.h>
#include <algorithm>
#include "resource.h"

static const point near_cell[] = { point(0,1), point(1,1), point(1,0), point(1,-1), point(0,-1), 
	point(-1,-1), point(-1,0), point(-1,1) };

const point* PlayerCharacter::nears = near_cell;
const int PlayerCharacter::near_count = 8;

PlayerCharacter::PlayerCharacter(unsigned ticks) : ACharacterAnimated(ticks), _lost(false)
{
	_anim1 = _anim = new CharacterSpriteSheet(DEFAULT_POINTF,ID_PLAYER);
	_anim2 = new CharacterSpriteSheet(DEFAULT_POINTF,ID_PLAYER_2);
}

PlayerCharacter::~PlayerCharacter()
{ 
	_anim = _anim1;
	delete _anim2; 
}

float PlayerCharacter::distance(pointf a, pointf b)
{
	return sqrt(pow((a-b).x,2) + pow((a-b).y,2)); 
}

float PlayerCharacter::distance(point a, point b)
{
	return sqrt(pow(float((a-b).x),2) + pow(float((a-b).y),2));
}

void PlayerCharacter::next_step()
{
	if (_target_cell == _current_cell) 
	{
		_lost = false;
		_way.clear();
		_bad_way.clear();
		EndMoving();
		return;
	}
	float min = 1000000; int j = -1;
	for(int i=0;i<near_count;i++)
		if (_b->InArea(_current_cell + nears[i]) && _b->IsCFree(_current_cell + nears[i])) {
			if (!_lost)
			{

				if (!_bad_way.empty() && std::find(_bad_way.begin(), _bad_way.end(), _current_cell+nears[i]) != _bad_way.end()) continue;
				if (!_way.empty() && std::find(_way.begin(), _way.end(), _current_cell+nears[i]) != _way.end()) continue; 

#define ON_SCREEN 0
#if ON_SCREEN
				float d = distance(_b->CCenter(_current_cell+nears[i]),_b->CCenter(_target_cell));
#endif
#if !ON_SCREEN
				float d = distance(_current_cell+nears[i],_target_cell);
#endif


				if (min>d) 
				{
					min = d;
					j = i;
				} else if (min == d) j = (rand()%2 == 0) ? j : i;
			}
			else 
			{
				if (!_way.empty()) 
				{
					_next_cell = _way.front(); 
					_bad_way.push_front(_current_cell);
					_way.pop_front();
					_lost = false;
					_isMoving = true;
					return; 
				} else 
				{
					_target_cell = _next_cell = _current_cell;
					EndMoving();
				}
			}

		}
		if (j>=0)
		{
			_next_cell = _current_cell + nears[j];
			_way.push_front(_current_cell);
			_isMoving = true; 
			playAnimation(getAnimationType(j));
		} else
			if (!_lost) 
			{ _lost = true; _isMoving = true; }
}



void PlayerCharacter::SetTargetCell(point cell)
{
	_target_cell = cell;
	_way.clear();
	_bad_way.clear();
	next_step();
}

void PlayerCharacter::Reset(point start, point finish /*= DEFAULT_POINT*/)
{
	_current_cell = start;
	_next_cell = _target_cell = _current_cell;
	_isMoving = false;
	SetPosition(_b->CCenter(_current_cell));
	_anim->Pause();
}

EAnimation PlayerCharacter::getAnimationType(int i)
{
	switch(i) {
	case 0:	case 1: return EAnimation::Right;
	case 2: case 3: return EAnimation::Down;
	case 4: case 5: return EAnimation::Left;
	case 6: case 7: return EAnimation::Up;
	}
}

void PlayerCharacter::end_moving_handler()
{
	if (_target_cell == _current_cell) 
	{
		_isMoving = false;
		stopAnimation(); 
	}
}

void PlayerCharacter::ResetSheet(bool isFirst /*= true*/)
{
	if (isFirst) _anim = _anim1;
	else _anim = _anim2;
}


