#pragma once

#include "ConsoleIO.h"
#include "Interfaces.h"
#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Graphics/Text.hpp>

class BoardInfo {
protected:
	ushort _health;
	ushort win_count;
	ushort lose_count;
	ushort level;
	ushort guard;
	ushort balls;
	ushort boss;
	const ushort guard_max;
	const ushort balls_max;
	const ushort boss_max;
	const ushort portal_max;
	ushort portals;
	ushort portal_count;
	EBonus _cur_bonus;

public:

	BoardInfo();
};

enum EMusic { Win, Lose, Boss, TakePortal, Backgraund, TakeBonus, IronMan };

class GUIBoard : public BoardInfo, AConsoleIO {
protected:
	
	BoardInfo bi;

	sf::Font font;

	sf::Text text_left;
	sf::Text text_right;

	sf::SoundBuffer  buffer_backgroud;
	sf::SoundBuffer  buffer_lose;
	sf::SoundBuffer  buffer_win;
	sf::SoundBuffer  buffer_boss;
	sf::SoundBuffer  buffer_portal;
	sf::SoundBuffer  buffer_bonus;
	sf::SoundBuffer  buffer_ironman;

	sf::Sound sound_backgroud;
	sf::Sound sound_lose;
	sf::Sound sound_win;
	sf::Sound sound_boss;
	sf::Sound sound_portal;
	sf::Sound sound_bonus;
	sf::Sound sound_ironman;

public:
	GUIBoard();

	void Win();
	void Lose();
	bool NewBoss();
	void Restart();
	bool OnPortal();

	void PlayMusic(EMusic musicType);
	void DrawGUI(sf::RenderWindow& rw);

};