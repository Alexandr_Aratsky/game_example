#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Texture.hpp>


typedef sf::Vector2f pointf;
typedef sf::Vector2i point;
typedef unsigned short ushort;

enum EAnimation { Up, Down, Left, Right, Hold };
enum EBonus { None, Portal, Bonus_1, Bonus_2, Bonus_3, Health, Grave};
enum ETex { Default = 1<<0, Floor = 1<<1, WallLeft = 1<<2, WallRight = 1<<3, Character = 1<<4, Bonus = 1<<5 };

class ITexture {
public:
	virtual void LoadTexture(const sf::Texture& texture, ushort texType) = 0;
};

class IDrowable {
public: 
	virtual void Draw(sf::RenderWindow& rw) = 0;
};

class IClickable {
public:
	virtual void MouseMove(int x, int y) = 0;
	virtual void MouseClickL(int x, int y) = 0;
	virtual void MouseClickR(int x, int y) = 0;
};

class IBoardInterface {
public:
	virtual pointf CCenter(point cell) = 0;
	virtual bool InArea(point cell) = 0;
	virtual bool IsCFree(point cell) = 0;
	virtual void TactEnd() = 0;
	virtual point PlayerCell() = 0;
	virtual void TryHold(point cell) = 0;
};
