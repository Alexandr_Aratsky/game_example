#include "ACharacter.h"

class GuardCharacter : public ACharacterAnimated {
private:

	bool _from1to2;

protected:

	point _a1; 
	point _a2;

	virtual void next_step();
	virtual void end_moving_handler();

public:
	GuardCharacter(unsigned ticks);
	virtual ~GuardCharacter();

	virtual void Reset(point start, point finish = DEFAULT_POINT);

};

class BossCharacter : public ACharacterAnimated {
private:

protected:

	virtual void end_moving_handler();
	virtual void next_step();

public:

	BossCharacter(unsigned ticks);
	virtual ~BossCharacter();

	virtual void Reset(point start, point finish = DEFAULT_POINT);

};

class Fireball : public ACharacterTexture {
private:

	bool _from1to2;

protected:

	point _a1; 
	point _a2;

	virtual void next_step();
	virtual void end_moving_handler();

public:

	Fireball(unsigned ticks);
	void LoadTexture(const sf::Texture& texture, ushort texType);

	virtual void Reset(point start, point finish = DEFAULT_POINT);

};


