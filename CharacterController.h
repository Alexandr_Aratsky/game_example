#pragma once

#include "ACharacter.h"
#include "Interfaces.h"
#include <SFML/System/Clock.hpp>

class CharacterController : public IBoardInterface
{
private:
	sf::Clock _timer;

	void update_position(ACharacter* ch);
	void was_moved(ACharacter* ch);

public:

	CharacterController();
	~CharacterController() { }

	void ConotrollerRestart();
	bool IsTactEnd();

	void Update(ACharacter* ch);
	void UpdateArray(ACharacter** ch_arr, int n);
};
