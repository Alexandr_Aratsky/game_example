#include "ACharacter.h"


class GuardCharacterSprite : public CharacterSprite {
protected:
	sf::Texture _tex;
	virtual void loadAnimatedTex();

public:
	GuardCharacterSprite(pointf startPosition, unsigned int frameCount);

};

class GuardCharacter : public ACharacterAnimated {
private:
	bool _from1to2;
	IBoardInterface* _b;
protected:
	point _a1; 
	point _a2;
	virtual void next_step();
	point v_sign(point p);
public:
	GuardCharacter(unsigned ticks, IBoardInterface& b, point a1, point a2);

	virtual void end_moving_handler();

};


