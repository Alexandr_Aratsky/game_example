#pragma once

#include <list>
#include "ACharacter.h"
#include "Interfaces.h"
#include <SFML/System/Clock.hpp>

// ����� ��������� ����������� ���������� ICharacter �� ������ �������
class CharacterAnimator : public IBoardInterface
{
private:
	sf::Clock _timer;

	void update_position(ACharacter* ch);
	void was_moved(ACharacter* ch);

public:

	CharacterAnimator();

	void Restart();
	bool TactEnd();

	void Update(ACharacter* ch);
	void UpdateArray(ACharacter** ch_arr, int n);
};
