#include <SFML/System/Time.hpp>


class GameWindow {
private:

	static float _delta_time;
	static int _width;
	static int _height;

public: 
	static float Delta();
	static sf::Time DeltaTime();
	static void Delta_time(float val);

	static int Width();
	static void Width(int val);

	static int Height();
	static void Height(int val);
};