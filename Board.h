#pragma once
#include "Cell.h"
#include "Wall.h"
#include "Interfaces.h"
#include "GameWindow.h"
#include "ACharacter.h"
#include "PlayerCharacter.h"
#include "GUIBoard.h"
#include "CharacterController.h"
#include "ConsoleIO.h"
#include <list>

typedef unsigned int uint;

class Board : public GUIBoard, CharacterController,
	IDrowable, IClickable, ITexture {
private:
	sf::Clock _timer;
	bool _ironMan;

	uint _size;
	Cell*** _board;
	Wall** _walls;

	point _cur_cell;
	point _target;

	PlayerCharacter* _player;
	ACharacter** _guards;
	ACharacter** _balls;
	ACharacter** _bosses;

protected:

	virtual point getCell(pointf p);

	void reset_all();

public:

	Board(uint size);
	~Board();

	void Draw(sf::RenderWindow& rw);

	void MouseMove(int x, int y);

	void MouseClickL(int x, int y);

	void MouseClickR(int x, int y);

	void LoadTexture(const sf::Texture& texture, ushort texType = ETex::Default);

	pointf CCenter(point cell);

	virtual bool InArea(point cell);

	virtual bool IsCFree(point cell);

	virtual void TactEnd();

	virtual point PlayerCell();

	virtual void TryHold(point cell);

	void GameOver(bool isWin);

};