#pragma once

#include "Interfaces.h"
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/Sprite.hpp>

typedef sf::ConvexShape cell_shape;
typedef sf::Sprite cell_sprite;

class CellObject : public IDrowable {
protected:
	static point _size;
	static sf::Texture _bonus_1;
	static sf::Texture _bonus_2;
	static sf::Texture _bonus_3;
	static sf::Texture _portal;
	static sf::Texture _health;
	static sf::Texture _grave;
	void LoadTextures();
	
	EBonus _type;
	sf::Sprite _obj;

public:

	CellObject();

	EBonus Type() const;
	void Type(EBonus val);
	void SetPosition(pointf p);
	virtual void Draw(sf::RenderWindow& rw);
	
};


// ����� ������, ��� � ������ ������� ������������ ��� ����
class Cell : public IDrowable, ITexture
{
private:

	static float _width;
	static float _height;

	bool _isFree;
	bool _isLight;

	CellObject _bonus;

	pointf _center;

	cell_shape _shape;
	cell_sprite _sprite;

protected:

	virtual void config_shape();
	virtual void config_color();
	virtual void config_bonus(EBonus _bonusType);

public:

	Cell(pointf center);
	~Cell();

	static float Width();
	static void Width(float val);
	static float Height();
	static void Height(float val);

	bool IsFree() const;
	void IsFree(bool val);
	bool IsLight() const;
	void IsLight(bool val);

	void Draw(sf::RenderWindow& rw);

	pointf Center() const;
	void Center(pointf val);

	bool InCell(pointf p);
	static bool InCellArea(pointf p, pointf c, float w, float h);

	void LoadTexture(const sf::Texture& texture, ushort texType = ETex::Default);

	EBonus GetBonus();
	bool IsPortal();
	bool IsGrave();
	void SetBonus(EBonus bonusType);

};
